const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");


const port = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

mongoose.connect('mongodb+srv://admin:admin123@zuittbootcamp.4mcba.mongodb.net/capstone2-working?retryWrites=true&w=majority',
	
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;


db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => console.log("Successfully connected to MongoDB Atlas"));

app.use("/users", userRoutes);
app.use("/product", productRoutes);

app.listen(port, () => console.log(`Server running at port ${port}`));
