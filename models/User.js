const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'Firstname is required']
	},

	lastName: {
		type: String,
		require: [true, 'Lastname is required']
	},

	email: {
		type: String,
		required: [true, 'Email is required']
	},

	password: {
		type: String,
		required: [true, 'Password is required']
	},

	mobileNo: {
		type: String,
		required: [true, 'Mobile No. is required']
	},

	birthday: {
		type: String,
		required: [true, 'Birthday is required']
	},

	isAdmin: {
		type: Boolean,
		required: false
	}

})

module.exports = mongoose.model('User', userSchema)