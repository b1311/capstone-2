const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  
    userId : {
        type: String,
        required: [true, 'userId is required']
    },

    totalAmount : {
        type: Number,
        required: [false, 'totalAmount is required']
    },

    purchasedOn : {
        type: Date,
        required: new Date()
    },

    items : [{
		productId: String,
		quantity: Number
	}]
})

module.exports = mongoose.model('Order', orderSchema);







