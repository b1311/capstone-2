const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		birthday: reqBody.birthday,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
		
	})

	return newUser.save().then((user, error) =>{
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//User login

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)	
			
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})
}


// Set User as Admin

module.exports.setUserAsAdmin = (requestBodyIsActive, requestParamsUserId) => {

	let updatedUser = {
		isAdmin: requestBodyIsActive
	}

	return User.findByIdAndUpdate(requestParamsUserId, updatedUser).then((user, error) => {
		if(error){
			return false;
		} else {
			return 'Successfuly updated user as admin';
		}
	})
}

// Create Orders

module.exports.createOrder = (userId, reqBody) => {

	console.log(userId)

	let order = new Order({
		userId: userId,
		purchasedOn: new Date(),
		items: []
	})

	for(i=0; i < reqBody.length; i++){
		let item = {
			productId: reqBody[i].productId,
			quantity: reqBody[i].quantity
		}
		order.items.push(item);
	}

	return order.save().then(result => {
		return result
	})
}

//  Retrieve all orders

module.exports.retrieveAllOrders = () => {
	return Order.find().then(result => {
		return result
	})
}

// Retrieve user's orders

module.exports.retrieveUserOrders = (userId) => {
	return Order.find({userId: userId}).then(result => {
		if(result == null) {
			return false 
		} else {
		return result
		}
	})
}

