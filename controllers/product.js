const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");



// Create Product

module.exports.createProduct = (reqBody) => {

	let newProduct = new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: true,
		createdOn: new Date()
	})

	return newProduct.save().then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// Retrieve all active Products

module.exports.retrieveAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}


// Retrieve Single Product

module.exports.singleProduct = (requestParamsProductId) => {

	return Product.findById(requestParamsProductId)
	.then((product, error) =>{
		if(error){
			return false
		} else {
			return product
		}
	})
}

// Update product

module.exports.updateProduct = (requestParamsProductId, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(requestParamsProductId, updatedProduct)
	.then((product, error) =>{
		if(error){
			return false
		} else {
			return 'Product Successfully updated'
		}
	})
}



//Archive product

module.exports.archiveProduct = (requestBodyIsActive, requestParamsProductId) => {

	let archivedProduct = {
		isActive: requestBodyIsActive
	}

	return Product.findByIdAndUpdate(requestParamsProductId, archivedProduct)
	.then((product, error) =>{
		if(error){
			return false
		} else {
			return 'Successfully archived'
		}
	})
}