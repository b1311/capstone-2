const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');


// Route for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))

})

//Route for User Authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})

// Set User as Admin

router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.setUserAsAdmin(req.body.isAdmin, req.params.userId).then(resultFromController => 
		res.send(resultFromController))
})


// Create Order

router.post('/checkout', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(!userData.isAdmin){
		userController.createOrder(userData.id, req.body.items).then(resultFromController => 
			res.send(resultFromController));
	}
})


// Retrieve all orders

router.get('/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userController.retrieveAllOrders().then(resultFromController => 
			res.send(resultFromController));
	} else {
		res.send("{auth: failed}")
	}
})

// Retrieve user's order

router.get('/myOrders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.retrieveUserOrders(userData.id).then(resultFromController => 
		res.send(resultFromController));
})

module.exports = router;

